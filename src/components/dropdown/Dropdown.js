import React, { Component } from 'react';
// var FontAwesome = require('react-fontawesome');
// import onClickOutside from "react-onclickoutside";

class Dropdown extends Component {
  constructor(props){
    super(props)
    this.state = {
      listOpen: false,
      headerTitle: this.props.title
    }
  }

//   static getDerivedStateFromProps(nextProps){
//     const count = nextProps.list.filter(function(a) { return a.selected; }).length;
//     console.log(count)
// if(count === 0){
//       return {headerTitle: nextProps.title}
//     }
//     else if(count === 1){
//       return {headerTitle: `${count} ${nextProps.titleHelper}`}
//     }
//     else if(count > 1){
//       return {headerTitle: `${count} ${nextProps.titleHelper}s`}
//     }
//   }

  handleClickOutside(){
    this.setState({
      listOpen: false
    })
  }

  toggleList(){
    this.setState(prevState => ({
      listOpen: !prevState.listOpen
    }))
  }

  handleClick(id, key, val){
    this.props.toggleItem(id, key, val);
  }

  render() {
    const{list} = this.props
    const{listOpen, headerTitle} = this.state
    
    return(
      <div className="dd-wrapper">
        <div className="dd-header" onClick={() => this.toggleList()}>
          <div className="dd-header-title">
            {headerTitle}
            {listOpen
              ? <img src={require('../../assets/images/up-arrow.png')} className="img-dropdown" alt="up"/>
              : <img src={require('../../assets/images/down-arrow.png')} className="img-dropdown" alt="down" />
            }
          </div>
        </div>

        {listOpen && <ul className="dd-list">
          {list.map((item) => (
            <li className="dd-list-item" key={item.title} onClick={() => this.handleClick(item.id, item.key, item.value)}>
            {item.title} 
            {item.selected && <img src={require('../../assets/images/checked.png')} className="img-dropdown" alt="checked" />}
            </li>
            ))}
          </ul>}
        
      </div>
    )
  }
}

export default Dropdown;
