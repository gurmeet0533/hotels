import React, { Component } from 'react'

class Card extends Component {
  render() {
    const { hotel } = this.props;

    let ratingText = '';
    if(hotel.reviewsRating.rating>4){
      ratingText = 'Very Good';
    }else if(hotel.reviewsRating.rating>3){
      ratingText = 'Good';
    }else if(hotel.reviewsRating.rating>2){
      ratingText = 'Moderate';
    }else{
      ratingText = 'Poor';
    }

    var recommended = hotel.recommendedFor.split(',');
    let tags = [];
    for (let i = 0; i < 2; i++) {
      tags.push(<div className='single-tag' key={i}>{recommended[i]} traveller</div>);
    }

    var accessibility = hotel.accessibility;
    let nearby = [];
    for (let key in accessibility) {
      if (accessibility.hasOwnProperty(key)) {
        nearby.push(<span key={key}><span className="color-dark">{accessibility[key]}km</span> from {key}, </span>);
      }
    }

    var rating = hotel.starRating;
    let starRating = [];
    for (let i = 1; i <= 5; i++) {
      if(rating >= i){
        starRating.push(<img src={require('../../assets/images/star.png')} key={i} alt="*" />);
      }else{
        starRating.push(<img src={require('../../assets/images/star_empty.png')} key={i} alt="-" />);
      }
    }
    

    return (
      <div className="card">
        <div className="card-header">
          <img src={hotel.image} className="card-img" alt="{hotel.name}"/>
          <div className="hotel-details">
            {hotel.promoted === 'Y' ? (<span className="flag">Promoted</span>) : null}
            <div className="rating-box">
              <span className="rating-main"> { hotel.reviewsRating.rating } </span> / 5
              <br/>
              {ratingText}
            </div>
            <div className="tags">
              {tags}
            </div>
            <p className="ranking">
              {hotel.reviewsRating.text}
            </p>
          </div>
        </div>

        <div className="card-body">
          <div className="hotel-left-section">
            <h3 className="hotel-name">
				      <a href={hotel.link}>{hotel.name}</a> {starRating}
				    </h3>
            <p className="address">
				      {hotel.address} <span className="distance">| {hotel.nearby} </span>
				    </p>
            <p className="match-percent">
				    	<span className="color-teal text-semibold">{hotel.searchMatch}% Match : </span> 
              {nearby}...
				    </p>
            <div className="offer">
              {hotel.price.offer.type === 'deal' ? (<span className="deal">Deal</span>) : null}
              <span className="color-teal">{hotel.price.offer.text}</span><a href={hotel.price.offer.link} className="color-link"> MORE</a>
            </div>
          </div>
          <div className="price-section">
			    	<span className="price-striked"><strike>${hotel.price.main}</strike></span>
            <span className="price-tags">
              <span className="price-tag"><a href={hotel.link}>20%</a></span>
            </span>
            <p className="main-price">${hotel.price.discounted}</p>
            <p className="price-detail">Per Room/ Night</p>
            <div className="facilities">
              {hotel.freeCancellation === true ? (<span className="color-teal facility">Free Cancellation</span>) : null}
              {hotel.freeWifi === true ? (<span className="color-teal facility">Free Wifi</span>) : null}
            </div>
			    </div>
        </div>
      </div>
    )
  }
}

export default Card;
