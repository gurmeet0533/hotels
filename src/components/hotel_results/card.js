import React, { Component } from 'react'

class Card extends Component {
  render() {
    const { hotel } = this.props;
    return (
      <div className="card">
                {hotel.name}
                <br/>
                {hotel.mainImages[0]}
                <br/>
                {hotel.featured == 'Y' ? 'Promoted' : null}
                <br/>
                {hotel.starRating}
                <br/>
                {/* {hotel.reviewsSummary.map((review, val) => (
                  <div>
                    {review.rating}
                  </div>
                ))} */}
                {hotel.reviewsSummary[0].rating}/5
                <br/>
                Good
                <br/>
                {hotel.reviewsSummary[0].recommendedFor.split(',').map((tag, val) => (
                  <div key={val}>
                    {tag}
                  </div>
                ))}
                <br/>
                {hotel.displayFare.slashedPrice.value}
              </div>
    )
  }
}

export default Card;
