import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Card from '../card/Card';
import Dropdown from '../dropdown/Dropdown';

class HotelResults extends Component {

  constructor(props){
    super(props);
    this.state = {
      rating: '',
      starRating: [
        {
            id: 1,
            title: <span><img src={require('../../assets/images/star.png')} alt="*"/><img src={require('../../assets/images/star.png')} alt="*"/><img src={require('../../assets/images/star.png')} alt="*" /><img src={require('../../assets/images/star.png')} alt="*" /><img src={require('../../assets/images/star.png')} alt="*" /></span>,
            selected: false,
            value:5,
            key: 'starRating'
        },
        {
          id: 2,
          title: <span><img src={require('../../assets/images/star.png')} alt="*" /><img src={require('../../assets/images/star.png')} alt="*" /><img src={require('../../assets/images/star.png')} alt="*" /><img src={require('../../assets/images/star.png')} alt="*" /><img src={require('../../assets/images/star_empty.png')} alt="-" /></span>,
          selected: false,
          value:4,
          key: 'starRating'
        },
        {
          id: 3,
          title: <span><img src={require('../../assets/images/star.png')} alt="*" /><img src={require('../../assets/images/star.png')} alt="*" /><img src={require('../../assets/images/star.png')} alt="*" /><img src={require('../../assets/images/star_empty.png')} alt="-" /><img src={require('../../assets/images/star_empty.png')} alt="-"/></span>,
          selected: false,
          value:3,
          key: 'starRating'
        },
        {
          id: 4,
          title: <span><img src={require('../../assets/images/star.png')} alt="*" /><img src={require('../../assets/images/star.png')} alt="*" /><img src={require('../../assets/images/star_empty.png')} alt="-"/><img src={require('../../assets/images/star_empty.png')} alt="-"/><img src={require('../../assets/images/star_empty.png')} alt="-"/></span>,
          selected: false,
          value:2,
          key: 'starRating'
        },
        {
          id: 5,
          title: <span><img src={require('../../assets/images/star.png')} alt="*" /><img src={require('../../assets/images/star_empty.png')} alt="-"/><img src={require('../../assets/images/star_empty.png')} alt="-"/><img src={require('../../assets/images/star_empty.png')} alt="-"/><img src={require('../../assets/images/star_empty.png')} alt="-"/></span>,
          selected: false,
          value:1,
          key: 'starRating'
        }
      ],
      price: '',
      sort:[
        {
          id: 1,
          title: 'Price low to high',
          selected: false,
          value: 1,
          key: 'sort'
        },
        {
          id: 2,
          title: 'Price high to low',
          selected: false,
          value: 2,
          key: 'sort'
        }
      ]
    };
  }

  toggleSelected = (id, key, val) => {
    let temp = this.state[key];
    let i = id-1;
    let newRating;
    if(this.state.rating === val){
      newRating = '';
    }else{
      newRating = val;
    }
    for(let j = 0; j < 5; j++){
      if(this.state.rating !== val)
      temp[j].selected = false;
    }
    temp[i].selected = !temp[i].selected
    this.setState({
      [key]: temp,
      rating: newRating
    })
  }

  sort = (id,key,val) => {
    let temp = this.state[key];
    let i = id-1;
    let newRating;
    if(this.state.price === val){
      newRating = '';
    }else{
      newRating = val;
    }
    for(let j = 0; j < 2; j++){
      if(this.state.price !== val)
      temp[j].selected = false;
    }
    temp[i].selected = !temp[i].selected
    this.setState({
      [key]: temp,
      price: newRating
    })
  }

  render() {
    // console.log(this.state);
    let hotelListContent;
    const { hotels } = this.props;

    if(this.state.price === 1){
      hotels.sort(function(a, b){
        return a.price.discounted-b.price.discounted
      })
    }else{
      hotels.sort(function(a, b){
        return b.price.discounted-a.price.discounted
      })
    }

    //above line is equivalent to const hotels = this.props.hotels
    if(hotels){
      hotelListContent = (
        
          <div className="row">
            {hotels.map((hotel, value) => (
                (this.state.rating === hotel.starRating || this.state.rating === '' ) ? (<Card hotel={hotel} key={value}/>) : null
            ))}
          </div>
      )
    }else{
      hotelListContent = null;
    }

    return (
      <div className="container">
          <div className="row heading-section">
            <h2 className="main-heading">
              {this.props.area}
              <span className="heading-desc">({this.props.count} Hotels)</span>
            </h2>
            <div className="top-filters">
              <Dropdown
                // titleHelper="Location"
                title={[<b>Filter By:</b>, " Star Rating"]}
                list={this.state.starRating}
                toggleItem={this.toggleSelected}
              />

              <Dropdown
                // titleHelper="Location"
                title={[<b>Sort By:</b>, " Price"]}
                list={this.state.sort}
                toggleItem={this.sort}
              />
            </div>
          </div>
        {hotelListContent}
      </div>
    )
  }
}

HotelResults.propTypes = {
  hotels: PropTypes.array.isRequired
};

export default HotelResults;
