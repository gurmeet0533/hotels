import React, { Component } from 'react';
import './assets/css/app.css';
import data from'./assets/data/hotels.json';
import HotelResults from './components/hotel_results/HotelResults';

class App extends Component {

  constructor(){
    super();
    this.state = {
      hotelArea: '',
      hotelCount: '',
      hotels: []
    }
  }

  componentDidMount() {

    fetch("https://api.myjson.com/bins/ptqg6")
    .then(res => res.json())
    .then(
      (result) => {
        if(result.hotelList){
          this.setState({
            hotelArea: result.city,
            hotelCount: result.hotelCount,
            hotels: result.hotelList
          });
        }else{
          this.setState({
            hotelArea: data.city,
            hotelCount: data.hotelCount,
            hotels: data.hotelList
          });
        }
      },
      (error) => {
        console.log(`Following error occured while making hotels ajax ${error}`);
        this.setState({
          hotelArea: data.city,
          hotelCount: data.hotelCount,
          hotels: data.hotelList
        });
      }
    )

  }

  render() {
    return (
      <div>
        {this.state.hotels.length > 0 ? (<HotelResults hotels={this.state.hotels} count={this.state.hotelCount} area={this.state.hotelArea} />) : null }
      </div>
    );
  }
}

export default App;
